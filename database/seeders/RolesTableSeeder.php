<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Admin',
            'display_name' => 'Administrador',
        ]);
        Role::create([
            'name' => 'Employee',
            'display_name' => 'Empleado',
        ]);
        Role::create([
            'name' => 'Client',
            'display_name' => 'Cliente',
        ]);
    }
}
