<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->create([
            'name' => 'Faustino Vasquez Limon',
            'email' => 'fvasquez@local.com',
        ]);

        $user->assignRole(1);

        $user = User::factory()->create([
            'name' => 'Sebastian Vasquez Tenorio',
            'email' => 'svasquez@local.com',
        ]);
        $user->assignRole(2);
    }
}
